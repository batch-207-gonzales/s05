SELECT customerName FROM customers WHERE country = "Philippines";

SELECT customerName FROM customers WHERE country = "USA";

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

SELECT customerName FROM customers WHERE state IS NULL;

SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

SELECT orderNumber FROM orders WHERE comments LIKE "%DHL%";

SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

SELECT DISTINCT country from customers;

SELECT DISTINCT status from orders;

SELECT customerName, country from customers WHERE country IN ("USA", "France", "Canada");

SELECT firstName, lastName, city FROM offices JOIN employees ON offices.officeCode = employees.officeCode WHERE city = "Tokyo";

SELECT customerName FROM customers JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE firstName = "Leslie" AND lastName = "Thompson";

SELECT productName, quantityInStock FROM products WHERE productLine = "planes" AND quantityInStock < 1000;

SELECT customerName FROM customers WHERE phone LIKE "+81%";

SELECT COUNT(*) FROM customers WHERE country = "UK";

/*STRETCH GOALS*/
SELECT productName, customerName from products JOIN orderdetails ON products.productCode = orderdetails.productCode JOIN orders ON orderdetails.orderNumber = orders.orderNumber JOIN customers ON orders.customerNumber = customers.customerNumber WHERE customerName = "Baane Mini Imports";

SELECT employee.lastName, employee.firstName FROM employees employee JOIN employees supervisor ON employee.reportsTo = supervisor.employeeNumber WHERE supervisor.firstName = "Anthony" AND supervisor.lastName = "Bow";

SELECT productName, MAX(MSRP) FROM products GROUP BY MSRP DESC LIMIT 1;

SELECT productLine, COUNT(*) FROM products GROUP BY productLine;

SELECT COUNT(*) FROM orders WHERE status = "Cancelled";